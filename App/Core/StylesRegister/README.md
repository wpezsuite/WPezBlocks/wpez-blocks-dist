## WPezClasses: Styles Register

__Register your styles in WordPress The ezWay.__

Because arrays are easier to configure, reuse, etc. They (i.e., the arrays) can also be filtered, and manipulated in ways that (hard) coding such things can't be.

Make your team and your users happy; leave the WordPress status quo behind (i.e., stop hardcoding your enqueue'd styles). Please. 

This class also integrates the WP function wp_style_add_data(), to help consolidate / cut back on sprawl.   

> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Simple Example

_Recommended: Use the WPezClasses autoloader (link below)._

    $arr_styles = [
        'foo' => [
            'active' => true,
            'handle' => 'foo-css',
            'src' => 'https://cdn.xyz/foocss',
            'deps => [],
            'ver' => 'v1.2.3',
            'media' => 'all',
            'hooks' => ['front', 'login'],
            'add_data_key' => ''
            'add_data_value' => ''
        
        ],
        'bar' => [
            ...
        ]
    ];
    
**'active'** - Simple bool on / off switch. Default: true; Optional (bool).

**'handle'** - Same as stock WP. *Required (string).

**'src'** - Same as stock WP. Default: false; Optional (string).

**'deps'** - Same as stock WP. Default: []; Optional (array).

**'ver'** - Same as stock WP. Default: false; Optional (string).

**'in_footer'** - Same as stock WP. Default: false; Optional (bool).

**'hooks'** - This script is associated with which hooks. Valid values: 'admin', 'front', 'login', 'block', 'block_admin', 'block_front'. Default: []; Optional (array of strings).

**'add_data_key'** - Valid values: 'conditional', 'rtl', 'suffix', 'alt' and 'title'. Default: false. Optional (string)

**'add_data_value'** - String containing the CSS data to be added. Default: false; Required if using 'add_data_key' (string).

    use \WPezSuite\WPezClasses\StylesRegister\ClassStylesRegister;
    use \WPezSuite\WPezClasses\StylesRegister\ClassHooks;

    $new_sr = new ClassStylesRegister();
    // push them one by one
    foreach ( $arr_styles as $arr_style){
        $new_sr->pushStyle($arr_style)
    }
    
    // or load them in bulk
    $new_sr->loadStyles($arr_scripts)
    
    // Once you have everything in place, it's hook'em time...
    $new_hooks = new ClassHooks($new_sr);
    $new_hooks->register()
    

### FAQ

**1) Why?**

Because configuring > coding.

Ultimately, arrays are so much easier to work with. If it helps, think of each script as an object with a finite handful of properties. Objects and properties can be updated, etc.


**2) Can I use this in my plugin or theme?**

Yes, but to be safe, please change the namespace. 


 __3) - I'm not a developer, can we hire you?__
 
Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 




### HELPFUL LINKS

 - https://gitlab.com/wpezsuite/WPezClasses/WPezAutoload
 
 - https://gitlab.com/wpezsuite/WPezClasses/ScriptsRegister

 - https://developer.wordpress.org/reference/functions/wp_enqueue_script

 - https://developer.wordpress.org/reference/functions/wp_style_add_data

 - https://codex.wordpress.org/Plugin_API/Action_Reference/wp_enqueue_scripts

 - https://codex.wordpress.org/Plugin_API/Action_Reference/admin_enqueue_scripts

 - https://codex.wordpress.org/Plugin_API/Action_Reference/login_enqueue_scripts
 
 - https://developer.wordpress.org/reference/hooks/enqueue_block_assets
 
 - https://developer.wordpress.org/reference/hooks/enqueue_block_editor_assets


### TODO

- Provide a better example(s)


### CHANGE LOG

- v0.6.0 - 20 May 2019
    - CHANGED: Now using the WPezClasses Hooks pattern
    - CHANGED: Added support for Gutenberg registering + enqueueing

- v0.5.0 - 11 March 2019
    - Some version of this has been kicking around in my toolbox for quite some time. So it's a new repo and a new version but a not-so-new idea. 