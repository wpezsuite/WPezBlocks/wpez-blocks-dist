<?php

namespace WPezBlocksExp\App\Plugin\Blocks;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}



class ClassRenderCallback {

    protected $_str_plugin_dir;
    protected $_str_plugin_url;
    protected $_str_blocks_namespace;
    protected $_str_blocks_namespace_safe;


    public function __construct($arr_args = false) {

        if ( is_array( $arr_args )){

            $this->setPropertyDefaults( $arr_args );


        }
    }

    protected function setPropertyDefaults($arr_args){

        $this->_str_plugin_dir = $arr_args['plugin_dir'];
        $this->_str_plugin_url = $arr_args['plugin_url'];
        $this->_str_blocks_namespace = trim($arr_args['blocks_namespace']);
        $this->_str_blocks_namespace_safe = trim($arr_args['blocks_namespace_safe']);

    }

    public function renderCallback($attributes, $context){


        if ( isset($attributes[ 'blockName'] )) {

            $str_block_name = trim($attributes['blockName']);
            $str_block_name_safe = str_replace('-', '_', $str_block_name);

            switch ($str_block_name){

                case 'text-heading':

                    // TODO - add filter to adjust defaults
                    $arr_ani_block = [
                        'blockAniName' => 'none',
                        'blockAniDuration' => 'none',
                        'blockAniDelay' => 'none',
                        'blockAniIterations' => '1',
                        'blockAniEasing' => 'ease',
                        'blockAniEleAnchor' => 'center',
                        'blockAniWinAnchor' => 'bottom',
                        'blockAniOffset' => 'none',

                    ];
                    // TODO - add filter to adjust defaults
                    $arr_ani_box = [
                        'boxAniName' => 'none',
                        'boxAniDuration' => 'none',
                        'boxAniDelay' => 'none',
                        'boxAniIterations' => '1',
                        'boxAniEasing' => 'ease',
                        'boxAniEleAnchor' => 'center',
                        'boxAniWinAnchor' => 'bottom',
                        'boxAniOffset' => 'none',
                    ];

                    // TODO - add filter to adjust defaults
                    $arr_ani_content = [
                        'contentAniName' => 'none',
                        'contentAniDuration' => 'none',
                        'contentAniDelay' => 'none',
                        'contentAniIterations' => '1',
                        'contentAniEasing' => 'ease',
                        'contentAniEleAnchor' => 'center',
                        'contentAniWinAnchor' => 'bottom',
                        'contentAniOffset' => 'none',
                    ];

                    $arr_attrs_defs = [
                        'featureSet' => '01',
                        'textDropCap' => false,
                    ];

                    $attributes = array_merge($arr_attrs_defs, $attributes);

                    // work the ani magic only if we need it
                    if ( $attributes['featureSet'] === '99' ) {

                        $arr_ani_block = array_merge($arr_ani_block, array_intersect_key($attributes, $arr_ani_block));

                        $arr_ani_box = array_merge($arr_ani_box, array_intersect_key($attributes, $arr_ani_box));

                        $arr_ani_content = array_merge($arr_ani_content, array_intersect_key($attributes, $arr_ani_content));
                    }

                    // pull out the stuff we don't want to automatically make into classes (i.e., what will be the attrs)
                    $arr_attrs_oth = [
                        'className' => '',
                        'blockAnchor' => '',
                        'contentRichText' => ''
                    ];
                    $arr_attrs_oth = array_merge(  $arr_attrs_oth, array_intersect_key( $attributes, $arr_attrs_oth));

                    // remove all of the above from the attrs. not really an unset but kinda :)
                    $arr_unset = array_merge( $arr_ani_block,  $arr_ani_box,  $arr_ani_content,  $arr_attrs_oth);
                    $arr_attrs = array_diff_key($attributes, $arr_unset);

                    // Now any other "pre-processing"

                    if ( $arr_attrs['textDropCap'] === true ){
                        $arr_attrs['textDropCap'] = 'true';
                    } else {
                     //   $arr_attrs['textDropCap'] = 'false';
                    }

                    $arr_block_args = [
                        'block_name' => $str_block_name,
                        'attributes' => $attributes,
                        'context' => $context,
                        'attrs' => $arr_attrs,
                        'attrs_oth' => $arr_attrs_oth,
                        'ani_block' => $arr_ani_block,
                        'ani_box' => $arr_ani_box,
                        'ani_content' => $arr_ani_content
                    ];

                    $str_ret = false;
                    return apply_filters($this->_str_blocks_namespace_safe . '_' . $str_block_name_safe, $str_ret, $arr_block_args);

                default:

                    return '';

            }

        } else {

            // return '456 - ';
        }


    }





}
