<?php

namespace WPezBlocksExp\App\Plugin\Blocks;

// No WP? Die! Now!!
if (!defined('ABSPATH')) {
    header('HTTP/1.0 403 Forbidden');
    die();
}

use WPezBlocksExp\App\Plugin\Blocks\ClassRenderCallback as RegCB;


class ClassRegisterBlockType
{

    protected $_str_plugin_dir;
    protected $_str_plugin_url;
    protected $_str_blocks_namespace;
    protected $_str_blocks_namespace_safe;
    protected $_arr_blocks;
    protected $_new_reg_cb;

    public function __construct($arr_args = false)
    {

        if (is_array($arr_args)) {

            $this->setPropertyDefaults($arr_args);
            // we're going to push all the WPezBlocks to the same render_callback
            // and then let that do the "pre-processing" and then the set apply_filters()
            $this->_new_reg_cb = new RegCB($arr_args);

        }
    }

    protected function setPropertyDefaults($arr_args)
    {

        $this->_str_plugin_dir = $arr_args['plugin_dir'];
        $this->_str_plugin_url = $arr_args['plugin_url'];
        $this->_str_blocks_namespace = trim($arr_args['blocks_namespace']);
        $this->_str_blocks_namespace_safe = trim($arr_args['blocks_namespace_safe']);

        $this->_arr_blocks = [];

    }

    public function setBlocks($arr = false)
    {

        if (is_array($arr)) {
            $this->_arr_blocks = $arr;
            return true;
        }
        return false;
    }

    public function register()
    {

        foreach ($this->_arr_blocks as $block => $bool) {

            if (is_string($block) && $bool !== false) {

                register_block_type(
                    $this->_str_blocks_namespace . '/' . trim($block),
                    ['render_callback' => [$this->_new_reg_cb, 'renderCallback'],
                        'attributes' => [
                            'blockName' => [
                                'default' => trim($block)
                            ]
                        ]
                    ]

                );

            }
        }
    }


}
