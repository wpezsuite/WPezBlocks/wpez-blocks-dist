<?php

namespace WPezBlocksExp\App\Plugin\Enqueue;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

use WPezBlocksExp\App\Core\ScriptsRegister\ClassScriptsRegister as ScriptsReg;
use WPezBlocksExp\App\Core\ScriptsRegister\ClassHooks as ScriptsHooks;
use WPezBlocksExp\App\Core\StylesRegister\ClassStylesRegister as StylesReg;
use WPezBlocksExp\App\Core\StylesRegister\ClassHooks as StylesHooks;


class ClassEnqueue {

    protected $_str_plugin_dir;
    protected $_str_plugin_url;
    protected $_str_path_slug;
    protected $_arr_scripts;
    protected $_arr_styles;


    public function __construct($arr_args = false) {

        if ( is_array( $arr_args )){

            $this->setPropertyDefaults( $arr_args );

            $this->scripts();
            $this->styles();
        }
    }

    protected function setPropertyDefaults($arr_args){

        $this->_str_plugin_dir = $arr_args['plugin_dir'];
        $this->_str_plugin_url = $arr_args['plugin_url'];
        $this->_str_path_slug = '/App/assets/dist';

        $this->_str_script_handle_primaray = 'wpezblocks-js';

        $this->_arr_scripts = [];
        $this->_arr_styles = [];


    }

    protected function scripts( $bool = true){

        if ( $bool === false ){
            return;
        }

        $block_path = '/js/editor.blocks.js';

        $this->_arr_scripts[] = [
            'handle' => $this->_str_script_handle_primaray,
            'src' =>  $this->_str_plugin_url . $this->_str_path_slug . $block_path,
            'deps' => [ 'wp-i18n', 'wp-element', 'wp-blocks', 'wp-components', 'wp-editor' ],
            'ver' => filemtime( $this->_str_plugin_dir . $this->_str_path_slug . $block_path ),
            'hooks' => ['block_admin']
        ];

        $block_path = '/js/frontend.blocks.js';


        $this->_arr_scripts[] = [
            'handle' => 'wpezblocks-front-js',
            'src' =>  $this->_str_plugin_url . $this->_str_path_slug . $block_path,
            'deps' => ['wpezblocks-aos-js'],
            'ver' => filemtime( $this->_str_plugin_dir .  $this->_str_path_slug . $block_path ),
            'hooks' => ['block_front']
        ];

        $new_rs = new ScriptsReg();

        $new_rs->loadScripts($this->_arr_scripts);

        $new_sh = new ScriptsHooks($new_rs);
        $new_sh->register();

    }

    protected function styles( $bool = true){

        if ( $bool === false ){
            return;
        }

        $style_path = '/css/blocks.editor.css';

        $this->_arr_styles[] = [
            'handle' => 'wpez-blocks-editor-css',
            'src' =>  $this->_str_plugin_url . $this->_str_path_slug . $style_path,
            'deps' => [],
            'ver' => filemtime( $this->_str_plugin_dir . $this->_str_path_slug . $style_path ),
            'hooks' => ['block_admin']
        ];

        $style_path = '/css/blocks.style.css';

        $this->_arr_styles[] = [
            'handle' => 'wpez-blocks-css',
            'src' =>  $this->_str_plugin_url . $this->_str_path_slug . $style_path,
            'deps' => [],
            'ver' => filemtime( $this->_str_plugin_dir . $this->_str_path_slug . $style_path ),
            'hooks' => ['block']
        ];

        $new_rs = new StylesReg();
        $new_rs->loadStyles($this->_arr_styles);

        $new_sh = new StylesHooks($new_rs);
        $new_sh->register();

    }


}

