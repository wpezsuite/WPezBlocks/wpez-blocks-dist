<?php

namespace WPezBlocksExp\App;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

use WPezBlocksExp\App\Plugin\Enqueue\ClassEnqueue as Enqueue;
use WPezBlocksExp\App\Plugin\Blocks\ClassRegisterBlockType as RBT;


class ClassPlugin {

    protected $_arr_blocks;

    public function __construct($arr_args = false) {

        if ( is_array( $arr_args )){

            $this->setPropertyDefaults($arr_args);

            $this->enqueue($arr_args);
            $this->registerBlockType($arr_args);

        }
    }

    protected function setPropertyDefaults($arr_args = []){

        $this->_arr_blocks = [

            'text-heading' => true,

        ];

    }

    protected function enqueue($arr_args){

        new Enqueue($arr_args);

    }

    protected function registerBlockType($arr_args){

        $new_rbt  = new RBT($arr_args);
        $new_rbt->setBlocks($this->_arr_blocks);

        $new_rbt->register();

    }
}

