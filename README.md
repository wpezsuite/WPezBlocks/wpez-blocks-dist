# Warning: 
#### _This is a different, and innovative approach to Gutenberg Blocks. If you're a status quo kinda person then please prepared to have your thoughts provoked._


### "Well then. Who is the target audience?"

- **Agencies, Developers & Designers** that want to embrace Gutenberg but are concerned that offering clients too much choice is going to add unnecessary friction to the content editing experience. That is, WPezBlocks wants to leave things like border radius, padding, etc. up to the theme. Less decisions not only makes clients happy but it helps mitigate the potential for inconsistencies that the non-design-minded are prone to doing. 


- **Agencies, Developers & Designers** that believe in brand style guides and therefore need ways to guide the content editing experience; without removing all the creativity from the process. For example, WPezBlocks allows you to define particular color combos and those can be block-type-centric. That is, Heading can have its set of color combos and Quote a different set. Again, allow clients to focus on content, not require them to remember the many subtleties of their style guide / brand identity. If nothing else, color combos combine two (or more) color picking clicks into a single click. 


- **Agencies, Developers & Designers** that are annoyed at some of the Gutenberg features (or the lack there of). For example, core's Heading block allows for adding an anchor (i.e., CSS id) but other blocks do not. Being able to link to a particular div of content - regardless of what's within that div - is one of the things that makes the internet so wonderful and powerful. Another inconvenience of core Blocks is the lack of content width options. Why can't a Heading or Quote be Wide or Full width? Now they can be.


- **Agencies, Developers & Designers** that wouldn't mind introducing animation into their solutions. _Note: If you don't want to use animation you can turn it off._


- **Agencies, Developers & Designers** that are concerned about "forward compatibility". That is, plenty of other blocks are inlining exact values (e.g., border radius, font-size, etc.) instead of using an abstraction for values. This is why WPezBlocks uses buttons (e.g., S, M, L) more and sliders less. We don't want you to be stuck with a "hardcoded" value that might not be brand-centric in the future. The added benefit is clients don't have to worry about exact values and instead can click a button and keep moving. Then in your theme's CSS you define what S, M and L represents. Forward compatibility!


- **Agencies, Developers & Designers** that want flexibility and control if/when they need it. This is one of the keys reasons why the functionality has been decoupled into three separate plugins, each dedicated to a single responsibility (so to speak).
 
### Screenshots

It's probably best to read the caveats below first, but for the more visual type this might help. 

https://wpezsuite.gitlab.io/WPezBlocks/wpez-blocks-screenshots/

Updated: Now with an example of floats.


### Let's Go! The 3 Required Plugins 
 

**- Dist** - (which will eventually be renamed Editor) are the admin side Blocks. The blocks are registered here. The filter(s) that provide the hook for each 'render_callback' is also setup here. 

https://gitlab.com/wpezsuite/WPezBlocks/wpez-blocks-dist

**- Frontend** - This is the PHP that uses the 'render_callback' filter, takes that content and attributes (as collected by the Dist / Editor plugin), and presents it within your theme. If you don't like these "templates" you can use the filter(s) and roll your own.

https://gitlab.com/wpezsuite/WPezBlocks/wpez-blocks-frontend

**- Customize** - This is where your magic happens. (_Note: It's also a bit raw in terms of how it's put together._) In the main PHP file you'll see (some of) the Dist/Editor setting you can control. The two CSS files let's you customize your Editor and Frontend styles. 

https://gitlab.com/wpezsuite/WPezBlocks/wpez-blocks-dist-customize

 
### Very Important Caveats
 
- The only block that's completely built out is Heading. In the Add Block pallet, search for: wpez.
 
- Other text-centric Blocks such as Paragraph, List, etc. all follow a very similar pattern and will be easy to implement once this first wave of testing is completed. 
 
- You can use any theme for your testing; the screenshots use the Twenty Seventeen theme. At the moment, the default CSS is "tuned" for that theme.

- There are occasional minor inconsistencies between what you'll see in the editor and what will be rendered correctly on the frontend. This is mainly due to the difference in widths between the Editor and the Frontend. The Editor's CSS compensates for the width of the block inspector.

- FYI, Animations are implemented with AOS.js (https://michalsnik.github.io/aos/) and Animate.css (https://daneden.github.io/animate.css/).

- In Customize's .php file are only some of the settings/args you'll have control over. Yes, obviously, all that needs to be documented (since many of the underlying components are custom and more robust than what core provides).

- Update: The Drop Cap CSS is now in play.  Note: It gets wonky when the text align is not left. 


### Slightly Less Important Caveats

- The creative mindset of this approach was very much inspired by the plugin Aesop Story Engine (http://aesopstoryengine.com/) and associated themes. I still love ASE and thank God for its inspiration.

- Without a one-on-one demo and/or better docs you'll have to rely on the screenshots (link above) to get a sense for now to make the magic happen. 

- The Feature Set buttons (at the top of the Inspector sidebar) determine which other settings (or not) will be exposed. It's recommended you open up all three panels - Wrapper, Customize and Colors - and then change the Feature Set buttons to see what changes.

- For the presentation, a WPezBlock block has an outer wrapper (aka the Block), within that is the Box, and within that is the Content. Each of those has its relevant settings. And if Feature Set: Advanced, they can be animated separately. 

- Being aware of this nesting will help you understand the settings and how they impact the resulting design. For example, Block Height: Full and Box Height: Full naturally removes the Box Vert Align control. Also, Box Height: None will remove the Customize's Text Align Vertical. That is, if the box has no height (and assumes the height of the content it wraps) there's no need for Text Align Vertical.

- That said, Feature Set: Basic will be similar to core blocks (and not have such nesting). Basic is meant to be as minimal as possible (read: less decisions) without being too limiting (e.g., Heading now has a width setting - if you want it). Also notice that Colors > Color Combos is different for Feature Set: Basic vs the Extra or Advanced; though in theory they could be the same. This is a Designer's design decision, and WPezBlocks likes it that way.


### Finally, And Most Importantly

The tag line of this project is:  **_"Think outside The Block."_**

The arcing Vision is to not get sucked into thinking of Gutenberg as yet another page builder. Instead, utilize Gutenberg as a content-friendly customizable theme-extender that enables content editors to better tell their "stories" in fresh and engaging ways.  This project wants to empower clients, not overwhemlm them with a kitchen sink of sliders and other design-centric decisions. Designers and themes are better at that. There's no need to abandon them completely. There's no reason to force clients into being designers.

Put another way, this project leans more towards a design system, and away from being a page builder.


Some are certain to ask "Why would we want to do this? or say "We'd never want to do that!"  Fair enough. To each her/his own. This might not be a tool for you. We're all alright. 


On the other hand, the better question is:

"Now that we can do this, in this way, with this level of control, what different and interesting layouts/designs can we do for our more forward-thinking clients? What if..."

Again.

**_"Think outside The Block."_**






 